#!/usr/bin/env ruby

require 'rubygems'
require 'json'
require 'net/http'
require 'set'


class API
  def initialize(url)
    @http = Net::HTTP.new(url)
  end

  def send(name, args)
    request = args.to_json
    path = "/#{name}"
    p [path, request]
    resp = @http.post(path, request)
    data = resp.body
    result = JSON.parse(data)
    raise result["error"] if result.has_key?("error")
    return result
  end

  def register(name, email)
    ret = send("register", :name => name, :email => email)
    @hit = Set.new
    return @id = ret["id"]
  end

  def nuke(x, y)
    raise "Register first" if @id.nil?
    ret = send("nuke", :id => @id, :x => x, :y => y)
    @hit << [x, y] if ret["status"] == "hit"
    ret
  end

  def fields
    Array.new(10) { |i| Array.new(10) { |j| [i, j] } }.flatten(1)
  end

  def print_board
    for y in 0...10
      for x in 0...10
        if @hit.include?([x, y])
          print "x"
        else
          print "."
        end
      end
      print("\n")
    end
  end

  def play_simple
    fields.each do |x, y|
      p nuke(x, y)
    end
  rescue
    print_board
  end

  def play_random
    fields.sort { rand }.each do |x, y|
      p nuke(x, y)
    end
  rescue
    print_board
  end
end

api = API.new "battle.platform45.com"
api.register("Roman", "rkj@go2.pl")
api.print_board
api.play_random

api = API.new "battle.platform45.com"
api.register("Roman", "rkj@go2.pl")
api.play_simple
